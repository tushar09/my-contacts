<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Country;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use stdClass;

class InsertContactsController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(){

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(){
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request){

        $country = Country::firstOrNew(array('name' => $request -> get('country')));
        $country -> name = $request -> get('country');
        $country -> save();

        $response = new stdClass();
        $response -> success = true;
        $response -> message = "we are learning";


        $json = json_decode($request -> get('data'),true);
        foreach($json as $value){
            $contacts = new Contact();
            $contacts -> number = $value['contact_number'];
            $contacts -> name = $value['name'];
            $contacts -> country_id = $country -> id;
            $contacts -> save();
        }

        return response(json_encode($country -> id), 200)
            ->header('Content-Type', 'Application/json');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id){
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id){
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id){
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id){
        //
    }
}
