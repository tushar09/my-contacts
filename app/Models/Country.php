<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 17 Nov 2017 16:04:11 +0000.
 */

namespace App\Models;

use Eloquent;

/**
 * Class Country
 *
 * @property int $id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 *
 * @package App\Models
 */
class Country extends Eloquent{
    protected $table = 'country';

    protected $fillable = ['name'];
}
