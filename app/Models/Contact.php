<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 17 Nov 2017 16:04:11 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Contact
 *
 * @property int $id
 * @property string $name
 * @property string $number
 * @property int $country_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @package App\Models
 */
class Contact extends Eloquent{
    protected $casts = ['country_id' => 'int'];

    protected $fillable = ['name', 'number', 'country_id'];
}
